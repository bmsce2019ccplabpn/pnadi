#include <stdio.h>
int inp()
{
    int x; 
    scanf("%d\n",&x);
    return x; 
}
int find(int a, int b, int c) 
{
    int max=0;
    if(a>b && a>c)
        max=a;
    else if(b>a && b>c)
        max=b;
    else if(c>a && c>b)
        max=c;
    return max; 
}
void display(int max) 
{
    printf("Largest of the three numbers is %d\n",max); 
}
int main()
{
    int a,b,c,max;
    printf("Enter\na=");
    a=inp();
    printf("Enter\nb=");
    b=inp();
    printf("Enter\nc=");
    c=inp();
    max=find(a,b,c);
    display(max);
    return 0;
}