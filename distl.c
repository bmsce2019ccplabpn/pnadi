#include <stdio.h>
#include <math.h>
struct point
{
	float x;
	float y;
};
struct point input()
{
	struct point p;
	printf("Enter the point\n");
	scanf("%f,%f",&p.x,&p.y);
	return p;
}
float dist(struct point a,struct point b)
{
	float d=sqrt(pow((a.y-b.y),2)+pow((a.x-a.y),2));
	return d;
}
void disp(float d)
{
	printf("Distance between two points is %0.2f",d);
}
int main()
{
	struct point a,b;
    float d;
	a=input();
	b=input();
	d=dist(a,b);
	disp(d);
	return 0;
}
