#include <stdio.h>
int inp()
{
    int x;
    printf("Enter a number ");
    scanf("%d",&x);
    return x;
}
void input(int n,int a[n+1])
{
    for(int i=0;i<n;i++)
    {
        a[i]=inp();
    }
}
void add(int n,int a[n+1],int p,int num)
{
    for(int i=n;i>=p;i--)
    {
        a[i]=a[i-1];
    }
    a[p-1]=num;
}
void disp(int e)
{
    printf("%d\n",e);
}
void output(int n,int a[n+1])
{
    for(int i=0;i<n;i++)
    {
        disp(a[i]);
    }
}
int main()
{
    int n,p,num;
    n=inp();
    printf("\n");
    printf("Enter Array Elements\n");
    int a[n+1];
    input(n,a);
    printf("Original Array Elements are\n");
    output(n,a);
    printf("Enter position at which element has to be inserted\n");
    p=inp();
    printf("Enter the element to be inserted\n");
    num=inp();
    add(n,a,p,num);
    printf("Modified Array Elements are\n");
    output(n+1,a);
    return 0;
}
