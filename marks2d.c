#include <stdio.h>
int inp()
{
    int x;
    scanf("%d",&x);
    return x;
}
void input(int r1,int c1,int a[r1][c1])
{
    for(int i=0;i<r1;i++)
    {
        printf("Enter marks of student %d\n\n",i+1);
        for(int j=0;j<c1;j++)
        {
            printf("Enter marks for subject %d  ",j+1);
            a[i][j]=inp();
        }
    }
}
void high(int r1,int c1,int a[r1][c1],int max[c1])
{
    for(int i=0;i<c1;i++)
    {
        max[i]=a[0][i];
        for(int j=0;j<r1;j++)
        {
            if(max[i]<a[j][i])
            {
                max[i]=a[j][i];
            }
        }
    }
}
void disp(int c1,int max[c1])
{
    for(int i=0;i<c1;i++)
    {
        printf("Highest in Subject %d is %d\n",i+1,max[i]);
    }
}
int main()
{
    int r,c;
    r=5;
    c=3;
    int a[r][c],max[c];
    input(r,c,a);
    high(r,c,a,max);
    disp(c,max);
    return 0;
}
