#include <stdio.h>
int fact(int n)
{
    int f=1;
    for(int i=1;i<=n;i++)
    {
        f*=i;
    }
    return f;
}
void disp(int a)
{
    printf("Factorial is %d\n",a);
}
int main()
{
    int n,f;
    printf("Enter value of n ");
    scanf("%d\n",&n);
    f=fact(n);
    disp(f);
    return 0;
}
