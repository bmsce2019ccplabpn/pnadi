#include <stdio.h>
#include <math.h>
struct pt
{
    float x; 
    float y; 
};
struct pt input()
{
    struct pt p; 
    printf("Enter a point\n");
    scanf("%f",&p.x);
    scanf("%f",&p.y);
    return p; 
}
float dist(struct pt a, struct pt b)
{
    float c; 
    c=sqrt(pow((a.x-b.x),2)+pow((a.y-b.y),2));
    return c; 
}
void output(float a) 
{
    printf("Distance = %f\n",a);
}
int main()
{
    struct pt a,b;
    float c; 
    a=input();
    b=input();
    c=dist(a,b);
    output(c);
    return 0;
}