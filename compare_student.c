#include <stdio.h>
struct date
{
    int d;
    int m;
    int y;
};
struct student
{
    char n[15];
    float cgpa;
    struct date doj;
    float fees;
};
struct student input()
{
    struct student a;
    printf("Enter name : ");
    scanf("%s",a.n);
    printf("Enter CGPA : ");
    scanf("%f",&a.cgpa);
    printf("Enter DOJ in dd/mm/yyyy : ");
    scanf("%d/%d/%d",&a.doj.d,&a.doj.m,&a.doj.y);
    printf("Enter fees : ");
    scanf("%f",&a.fees);
    printf("\n");
    return a;
}
struct student highest(struct student s1, struct student s2)
{
    struct student h;
    if(s1.cgpa>s2.cgpa)
    {
        h=s1;
    }
    else
    {
        h=s2;
    }
    return h;
}
void display(struct student a)
{
    printf("Name : %s\n CGPA %f\n DOJ %d/%d/%d\n Fees %0.2f\n",a.n,a.cgpa,a.doj.d,a.doj.m,a.doj.y,a.fees);
}
int main()
{
    struct student s1,s2,h;
    s1=input();
    s2=input();
    h=highest(s1,s2);
    display(s1);
    display(s2);
    display(h);
    return 0;
}