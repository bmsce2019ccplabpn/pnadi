#include <stdio.h>
struct fr
{
    int n;
    int d;
};
struct fr input()
{
    struct fr f;
    printf("Enter the numerator\n");
    scanf("%d",&f.n);
    printf("Enter the denominator\n");
    scanf("%d",&f.d);
    return f;
}
struct fr comp(struct fr a,struct fr b)
{
    struct fr c;
    c.n=(a.n*b.d)+(a.d*b.n);
    c.d=a.d*b.d;
    return c;
}
struct fr sim(struct fr a)
{
    for(int i=1;i<=a.n;i++)
    {
        if(a.n%i==0 && a.d%i==0)
        {
            a.n=a.n/i;
            a.d=a.d/i;
        }
    }
    return a;
}
void out(struct fr a,struct fr b,struct fr c)
{
    if(c.n==c.d)
    {
        printf("%d/%d + %d/%d = 1\n",a.n,a.d,b.n,b.d);
    }
    else
    {
        printf("%d/%d + %d/%d = %d/%d\n",a.n,a.d,b.n,b.d,c.n,c.d);
    }
}
int main()
{
	struct fr a,b,c,d;
    a=input();
    b=input();
    c=comp(a,b);
    d=sim(c);
    out(a,b,d);
	return 0;
}
