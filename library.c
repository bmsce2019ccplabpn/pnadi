#include <stdio.h>
struct date
{
    int d;
    int m;
    int y;
};
struct book
{
    char n[15];
    char a[15];
    float price;
    struct date dop;
};
struct book input()
{
    struct student a;
    printf("Enter name of book : ");
    scanf("%s",a.n);
    printf("Enter name of author : ");
    scanf("%s",a.a);
    printf("Enter price : ");
    scanf("%f",&a.price);
    printf("Enter DOP in dd/mm/yyyy : ");
    scanf("%d/%d/%d",&a.dop.d,&a.dop.m,&a.dop.y);
    printf("\n");
    return a;

}
void display(struct book a)
{
    printf("Name of Book : %s\n Name of Author : %s\n Price %0.2f\n DOJ %d/%d/%d\n",a.n,a.a,a.price,a.dop.d,a.dop.m,a.dop.y);
}
int main()
{
    struct book b;
    b=input();
    display(b);
    return 0;
}