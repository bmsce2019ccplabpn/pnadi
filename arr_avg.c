#include <stdio.h>
int inp()
{
    int x;
    printf("Enter a number ");
    scanf("%d",&x);
    return x;
}
void input(int n,int a[n])
{
    for(int i=0;i<n;i++)
    {
        a[i]=inp();
    }
}
float comp(int n,int a[n])
{
    float av;
    int s=0;
    for(int i=0;i<n;i++)
    {
        s+=a[i];
    }
    av=s/(float)n;
    return av;
}
void disp(int e)
{
    printf("%d\n",e);
}
void output(int n,int a[n],float av)
{
    for(int i=0;i<n;i++)
    {
        disp(a[i]);
    }
    printf("Average is %0.2f\n",av);
}
int main()
{
    int n;
    n=inp();
    printf("\n");
    printf("Enter Array Elements\n");
    int a[n];
    float av;
    input(n,a);
    av=comp(n,a);
    printf("Array Elements are\n");
    output(n,a,av);
    return 0;
}
