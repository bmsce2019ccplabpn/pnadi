def inp():
    a=int(input("Enter first number: "))
    b=int(input("Enter second number: "))
    c=int(input("Enter third number: "))
    return a,b,c
def find(a,b,c):
    if(a>b and a>c):
        return a
    elif(b>a and b>c):
        return b
    else:
        return c
def disp(a,b,c,d):
    print("Greatest of %d,%d,%d is %d"%(a,b,c,d))
def main():
    x,y,z=inp()
    r=find(x,y,z)
    disp(x,y,z,r)
main()