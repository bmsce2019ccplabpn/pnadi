#include <stdio.h>
int input()
{
    int x;
    printf("\nEnter a value\n");
    scanf("%d",&x);
    return x;
}
int add(int x,int y)
{
    int r=x+y;
    return r;
}
void output(int x,int y,int z)
{
    printf("\n%d+%d=%d\n",x,y,z);
}
int main()
{
    int a,b,c;
    a=input();
    b=input();
    c=add(a,b);
    output(a,b,c);
    return 0;
}