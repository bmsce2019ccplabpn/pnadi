#include <stdio.h>
#include <math.h>
int inp()
{
    int x;
    printf("Enter a number\n");
    scanf("%d",&x);
    return x;
}
int len(int n)
{
    int i=0;
    while(n!=0)
    {
        n=n/2;
        i++;
    }
    return i;
}
void conv(int n,int a[])
{
    int i=0;
    while(n!=0)
    {
        a[i]=n%2;
        n=n/2;
        i++;
    }
}
int calc(int a[],int len)
{
    int sum=0;
    for(int i=len-1;i>=0;i--)
    {
        sum=sum*10+a[i];
    }
    return sum;
}
void disp(int n,int num)
{
    printf("%d is %d",n,num);
}
int main()
{
    int n,l,num;
    n=inp();
    l=len(n);
    int a[l];
    conv(n,a);
    num=calc(a,l);
    disp(n,num);
    return 0;
}
