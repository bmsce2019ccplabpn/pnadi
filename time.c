#include <stdio.h>
int inp()
{
    int x;
    scanf("%d",&x);
    return x;
}
int conv(int h,int m)
{
    int mm;
    mm=h*60+m;
    return mm;
}
void display(int h,int m,int mm)
{
    printf("%d:%d is %d minutes\n",h,m,mm);
}
int main()
{
    int h,m,mm;
    printf("Enter hours ");
    h=inp();
    printf("\nEnter minutes ");
    m=inp();
    mm=conv(h,m);
    display(h,m,mm);
    return 0;
}
