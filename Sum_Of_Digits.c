#include <stdio.h>
int inp()
{
   int x; 
   printf("Enter a number : ");
   scanf("%d\n",&x); 
   return x; 
}
int comp(int n) 
{
   int d,s=0;
   while(n!=0)
   {
      d=d%10;
      s=s+d;
      n=n/10;
   }
   return s;
}
void display(int n,int s) 
{
   printf("The sum of digits of %d is %d\n",n, s); 
}
int main()
{
   int n,s; 
   n=inp();
   s=comp(n);
   display(n,s);
   return 0;
}
