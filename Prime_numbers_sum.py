def inp():
        number_list=[]
        print("To end entering numbers, press 0")
        number=int(input("Enter the numbers "))
        while number != 0:
            number_list.append(number)
            number=int(input("Enter next number "))
            print(number_list)
        return number_list

def is_prime(number):
        if number==1:
                return False
        if number==2:
                return True
        if number%2==0:
                return False
        i=3
        while i<(number**0.5)+1:
                if number%i ==0:
                    return False
                i+=2
        return True

def find_prime(numbers):
        prime_numbers=[]
        for number in numbers:
                if is_prime(number):
                        prime_numbers.append(number)
        return prime_numbers
        
def sum_prime(prime):
        sum=0
        for number in prime:
                sum=sum+number
        return sum
    
def display(prime_num,sum):
        print(f"Sum of the following prime numbers {str(prime_num).strip()} is {sum}")
        
if __name__ == "__main__":
        numbers=[]
        numbers=inp()
        prime_numbers=[]
        prime_numbers=find_prime(numbers)
        sum=sum_prime(prime_numbers)
        display(prime_numbers,sum)
