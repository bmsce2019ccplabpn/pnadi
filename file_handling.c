#include <stdio.h>
#include <stdlib.h>
struct student
{
    char name[50];
    char section[2];
    int year;
    int roll;
    int marks;
};
int intin()
{
    int x;
    scanf("%d",&x);
    return x;
}
void take_input(FILE* op,int n)
{
    printf("\n");
    struct student info[n];
    op=fopen("INPUT.txt","ab");
    if(op==NULL)
    {
        printf("File didn't open\n");
        exit(0);
    }
    printf("Enter the details of students\n\n");
    for(int i=0;i<n;i++)
    {
        printf("Enter name of student\t");
        scanf("%s",info[i].name);
        printf("\n");
        printf("Enter section of the student\t");
        scanf("%s",info[i].section);
        printf("\n");
        printf("Enter the year to which the student belong\t");
        scanf("%d",&info[i].year);
        printf("\n");
        printf("Enter roll number of the student\t");
        scanf("%d",&info[i].roll);
        printf("\n");
        printf("Enter marks of student\t");
        scanf("%d",&info[i].marks);
        printf("\n");
    }
    fwrite(info,sizeof(info),1,op);
    fclose(op);
}
void search(char a[])
{
    printf("Enter the name of the student to search for :\t");
    scanf("%s",a);
}
int compare(char a[],char b[])
{
    int f=1,i=0;
    while(a[i]!='\0' || b[i]!='\0')
    {
        if(a[i]!=b[i])
        {
            f=0;
            break;
        }
        i++;
    }
    return f;
}
void show_output(char find[],FILE* op,int n)
{
    op=fopen("INPUT.txt","rb");
    struct student data[n];
    int c=0;
    fread(data,sizeof(data),1,op);
    for(int i=0;i<n;i++)
    {
        if(compare(data[i].name,find)==1)
        {
            printf("\n");
            printf("Section :\t%s",data[i].section);
            printf("\n");
            printf("Year :\t%d",data[i].year);
            printf("\n");
            printf("Roll number :\t%d",data[i].roll);
            printf("\n");
            printf("Marks:\t%d",data[i].marks);
            printf("\n");
            c++;
        }
    }
    if(c==0)
    {
        printf("Student record not found!!!\n");
    }
    fclose(op);
}
int main()
{
    FILE *op;
    int n;
    char a[50];
    printf("Enter the number of students\n");
    n=intin();
    take_input(op,n);
    search(a);
    show_output(a,op,n);
    return 0;
}
