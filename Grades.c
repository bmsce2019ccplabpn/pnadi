#include <stdio.h>
int input()
{
    int x;
    printf("Enter the marks : ");
    scanf("%d",&x);
    return x;
}
char compute(int x)
{
    char g;
    if(x>=85) 
        g='A';
    else if(x<85 && x>=75)
        g='B'; 
    else if(x<75 && x>=65)
        g='C';
    else if(x<65 && x>=55)
        g='D';
    else
        g='E';
    return g;
}
void output(char r)
{
    
    printf("Grade is : %c\n",r);
}

int main()
{
    int a;
    a=input();
    char c;
    c=compute(a);
    output(c);
    return 0;
}