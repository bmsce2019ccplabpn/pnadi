#include <stdio.h>
int inp()
{
    int x;
    printf("Enter a number ");
    scanf("%d",&x);
    return x;
}
void input(int n,int a[n])
{
    printf("Enter Array Elements\n");
    for(int i=0;i<n;i++)
    {
        a[i]=inp();
    }
}
int search(int n,int a[],int key)
{
    int f=0;
    for(int i=0;i<n;i++)
    {
        if(a[i]==key)
        {
            f++;
            display(key,i);
        }
    }
    if(f==0)
    {
        printf("%d not found\n",key);
    }
}
void display(int key,int p)
{
    printf("%d is found at %d\n",key,p+1);
}
int main()
{
    int n,key;
    printf("Enter the size of the array\n");
    n=inp();
    printf("\n");
    int a[n];
    input(n,a);
    printf("Enter the number to be searched\n");
    key=inp();
    printf("\n");
    search(n,a,key);
    return 0;
}
