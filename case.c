#include <stdio.h>
int in()
{
    int x;
    printf("Enter maximum size of string\t");
    scanf("%d",&x);
    return x;
}
void inp(char a[])
{
	printf("Enter a string\n");
	scanf("%s",a);
        printf("\n");
}
void conv(char a[])
{
	int c=0;
        while(a[c]!='\0')
	{
            if(a[c]>='A' && a[c]<='Z')
            {
                a[c]+=32;
            }
            else if(a[c]>='a' && a[c]<='z')
            {
                a[c]-=32;
            }
            c++;
	}
}
void disp(char a[])
{
	printf("%s\n",a);
}
int main()
{
	int n;
        n=in();
        char a[50];
        inp(a);
        printf("Original Text\t");
        disp(a);
        conv(a);
        printf("New Text\t");
        disp(a);
        return 0;
}
