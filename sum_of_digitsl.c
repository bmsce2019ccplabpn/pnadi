#include <stdio.h>
int inp()
{
	int x;
	printf("Enter a number ");
	scanf("%d",&x);
	return x;
}
int calc(int n)
{
	int s=0,d;
	while(n!=0)
	{
		d=n%10;
		s+=d;
		n/=10;
	}
	return s;
}
void display(int n,int s)
{
	printf("Sum of digits of %d is %d",n,s);
}
int main()
{
	int n,s;
	n=inp();
	s=calc(n);
	display(n,s);
	return 0;
}