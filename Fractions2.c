 #include <stdio.h>
struct fr
{
    int n;
    int d;
};
void input(struct fr *a,int nu)
{
    for(int i=0;i<nu;i++)
        {
            printf("Enter the numerator\n");
            scanf("%d",&a[i].n);
            printf("Enter the denominator\n");
            scanf("%d",&a[i].d);
        }
}
void display(struct fr *a,int nu)
{
    for(int i=0;i<nu;i++)
        {
            printf("%d/%d + ",a[i].n,a[i].d);
        }
}
struct fr comp(struct fr *a,int nu)
{
    struct fr c;
    c.n=0;
    c.d=1;
    for(int i=0;i<nu;i++)
    {
        c.n=(a[i].n*c.d)+(a[i].d*c.n);
        c.d=a[i].d*c.d;
    }
    return c;
}
struct fr sim(struct fr s)
{
    for(int i=s.n;i>0;i--)
    {
        if(s.n%i==0 && s.d%i==0)
        {
            s.n=s.n/i;
            s.d=s.d/i;
        }
    }
    return s;
}
void out(struct fr c)
{
    if(c.n==c.d)
    {
        printf(" = 1\n");
    }
    else    
    {
        printf(" = %d/%d\n",c.n,c.d);
    }
}
int main()
{
	int nu;
    printf("Enter the value of n\n");
    scanf("%d",&nu);
    struct fr a[nu],c,e;
    input(a,nu);
    display(a,nu);
    c=comp(a,nu);
    e=sim(c);
    out(e);
	return 0;
}