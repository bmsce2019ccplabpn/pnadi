def inp():
    a=int(input("Enter first number : "))
    b=int(input("Enter second number : "))
    return a,b
def add(a,b):
    c=a+b
    return c
def disp(a,b,c):
    print("%d+%d=%d"%(a,b,c))
def main():
    x,y=inp()
    z=add(x,y)
    disp(x,y,z)
main()