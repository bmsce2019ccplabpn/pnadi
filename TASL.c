#include <stdio.h>
int input()
{
    int x;
    printf("Enter a number : ");
    scanf("\n%d",&x);
    return x;
}
int max(int a,int b,int c)
{
    int max;
    max=((a>b)?((a>c)?a:c):((b>c)?b:c));
    return max;
}
int min(int a,int b,int c)
{
    int min;
    min=((a<b)?((a<c)?a:c):((b<c)?b:c));
    return min;
}
int tot(int a,int b,int c)
{
    int t=a+b+c;
    return t;
}
int avg(int a,int b,int c)
{
    int av=tot(a,b,c);
    av=av/3;
    return av;
}
void output(int x,int y,int t,int a)
{
    printf("\nLargest Number is : %d\n",x);
    printf("\nSmallest Number is : %d\n",y);
    printf("\nTotal of Numbers is : %d\n",t);
    printf("\nAverage of Numbers is : %d\n",a);
}
int main()
{
    int a,b,c,d,e,f,g;
    a=input();
    b=input();
    c=input();
    d=max(a,b,c);
    e=min(a,b,c);
    f=tot(a,b,c);
    g=avg(a,b,c);
    output(d,e,f,g);
    return 0;
}