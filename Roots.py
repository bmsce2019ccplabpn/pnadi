import math
def inp():
    print("Quadratic Equation : ax^2 + bx +c")
    a=int(input("Enter the value of a: "))
    b=int(input("Enter the value of b: "))
    c=int(input("Enter the value of c: "))
    return a,b,c
def check_roots(a,b,c):
    D=float(b*b-4*a*c)
    if D>=0.0:
        return True,D
    else:
        return False,D
def print_poss(p,D,a,b,c):
    if p==True:
        if(D==0):
            print("Quadratic Equation %dx^2 + %dx + %d has real and equal roots"%(a,b,c))
        else:
            print("Quadratic Equation %dx^2 + %dx + %d has real and distinct roots"%(a,b,c))
        roots(D,a,b)
    else:
        print("Quadratic Equation %dx^2 + %dx + %d has imaginary roots"%(a,b,c))
def roots(D,a,b):
    r1=((-b+math.sqrt(D))/(2*a))
    r2=((-b-math.sqrt(D))/(2*a))
    disp(r1,r2)
def disp(r1,r2):
    print("Roots are: %f and %f"%(r1,r2))
def main():
    x,y,z=inp()
    p,D=check_roots(x,y,z)
    print_poss(p,D,x,y,z)
main()
