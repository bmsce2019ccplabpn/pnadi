#include <stdio.h>
int inp()
{
	int x;
	printf("Enter a number ");
	scanf("%d",&x);
	return x;
}
void inp_arr(int n,int a[n])
{
	for(int i=0;i<n;i++)
	{
		a[i]=inp();
	}
}
void manu(int n,int a[n])
{
	int l=a[0],pl=0,s=a[0],ps=0;
	for(int i=0;i<n;i++)
	{
		if(a[i]>l)
		{
			l=a[i];
			pl=i;
		}
                if(a[i]<s)
		{
			s=a[i];
			ps=i;
		}
	}
        printf("Largest Number is %d and is at position %d\n",l,pl+1);
        printf("Smallest Number is %d and is at position %d\n",s,ps+1);
	int temp=a[pl];
        a[pl]=a[ps];
        a[ps]=temp;
}
void disp(int n,int a[n])
{
	for(int i=0;i<n;i++)
	{
		printf("%d\t",a[i]);
	}
	printf("\n");
}
int main()
{
	int n;
        printf("Enter Size of Array\n");
	n=inp();
	int a[n];
        printf("\n");
        printf("Enter Array Elements\n");
	inp_arr(n,a);
        printf("Original Array Elements are\n");
	disp(n,a);
	manu(n,a);
        printf("Modified Array Elements are\n");
	disp(n,a);
	return 0;
}
