#include <stdio.h>
int inp()
{
    int x;
    printf("Enter a number ");
    scanf("%d",&x);
    return x;
}
void input(int n,int a[n])
{
    for(int i=0;i<n;i++)
    {
        a[i]=inp();
    }
}
void disp(int e)
{
    printf("%d\n",e);
}
void output(int n,int a[n])
{
    for(int i=0;i<n;i++)
    {
        disp(a[i]);
    }
}
int main()
{
    int n;
    n=inp();
    printf("\n");
    printf("Enter Array Elements\n");
    int a[n];
    input(n,a);
    printf("Array Elements are\n");
    output(n,a);
    return 0;
}
