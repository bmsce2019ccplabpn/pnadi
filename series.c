#include <stdio.h>
int inp()
{
    int n;
    printf("Enter value of n ");
    scanf("%d\n",&n);
    return n;
}    
int fact(int n)
{
    int f=1;
    for(int i=1;i<=n;i++)
    {
        f*=i;
    }
    return f;
}
float ser(int n)
{
    float s=0.0;
    for(int i=1;i<=n;i++)
    {
        s+=(float)(i/(float)fact(i));
    }
    return s;
}
void disp(float a)
{
    printf("Sum is %f\n",a);
}
int main()
{
    int n;
    float s;
    n=inp();
    s=ser(n);
    disp(s);
    return 0;
}
