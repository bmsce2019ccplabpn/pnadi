#include <stdio.h>
int input()
{
    int x;
    printf("Enter a year : ");
    scanf("\n%d",&x);
    return x;
}
int leap(int x)
{
    int f=0;
    if((x%4==0 && x%100!=0)||(x%400==0))
    {
        f=1;
    }
    return f;
}
void output(int x,int y)
{
    if(x==1)
        {
            printf("\n%d is a leap year\n",y);
        }
    else if(x==0)
        {
            printf("\n%d is not a leap year\n",y);
        }
}
int main()
{
    int a,b;
    a=input();
    b=leap(a);
    output(b,a);
    return 0;
}