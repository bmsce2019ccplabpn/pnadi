import math
def inp():
    x=float(input("Enter angle in Degrees/nTo find: sin "))
    return x
def fact(a):
    fac=1
    for i in range(1,a+1):
        fac=fac*i
    return fac
def find_range(x):
    n=int(x/360)
    return (x-n*360)
def calc(x):
    x=(x/180)*(math.pi)
    i=0
    p=1
    sum=0
    while(i<20):
        if(i%2==0):
            sum=sum+(pow(x,p)/fact(p))
        else:
            sum=sum-(pow(x,p)/fact(p))
        i=i+1
        p=p+2
    return sum
def disp(x,sum):
    print("sin %0.2f = %f"%(x,sum))
def main():
    d=inp()
    e=find_range(d)
    res=calc(e)
    disp(d,res)
main()
