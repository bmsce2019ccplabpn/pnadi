#include <stdio.h>
int inp()
{
	int x;
	printf("Enter a number ");
	scanf("%d",&x);
	return x;
}
int only(int a,int b,int c)
{
	int l;
	if(a>b && a>c)
		l=a;
	else
		l=b;
	return l;
}
int nested(int a,int b,int c)
{
	int l;
	if(a>b)
		if(a>c)
			l=a;
	else if(b>a)
		if(b>c)
			l=b;
	else if(c>a)
		if(c>b)
			l=c;
	return l;
}
int ladder(int a,int b,int c)
{
	int l;
	if(a>b && a>c)
		l=a;
	else if(b>a && b>c)
		l=b;
	else if(c>a && c>b)
		l=c;
	return l;
}
int tern(int a,int b,int c)
{
	int l=(a>b?(a>c?a:c):(b>c?b:c));
	return l;
}
void disp(int l)
{
	printf("Largest of three numers = %d",l);
}
int main()
{
	int a,b,c,l,ch;
	a=inp();
	b=inp();
	c=inp();
	printf("Enter\n1. Only if\n2. Nested if\n3. Ladder if\n4. Ternary\n");
	ch=inp();
	switch(ch)
	{
		case 1:
			l=only(a,b,c);
			disp(l);
			break;
		case 2:
			l=nested(a,b,c);
			disp(l);
			break;
		case 3:
			l=ladder(a,b,c);
			disp(l);
			break;
		case 4:
			l=tern(a,b,c);
			disp(l);
			break;
		default:
			printf("WRONG CHOICE");
			break;
	}
	return 0;
}
