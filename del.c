#include <stdio.h>
int inp()
{
    int x;
    printf("Enter a number ");
    scanf("%d",&x);
    return x;
}
void input(int n,int a[n])
{
    for(int i=0;i<n;i++)
    {
        a[i]=inp();
    }
}
void del(int n,int a[n],int p)
{
    for(int i=p-1;i<n-1;i++)
    {
        a[i]=a[i+1];
    }
    a[n-1]=0;
}
void disp(int e)
{
    printf("%d\n",e);
}
void output(int n,int a[n])
{
    for(int i=0;i<n;i++)
    {
        disp(a[i]);
    }
}
int main()
{
    int n,p,num;
    n=inp();
    printf("\n");
    printf("Enter Array Elements\n");
    int a[n];
    input(n,a);
    printf("Original Array Elements are\n");
    output(n,a);
    printf("Enter position for which element has to be deleted\n");
    p=inp();
    del(n,a,p);
    printf("Modified Array Elements are\n");
    output(n-1,a);
    return 0;
}
