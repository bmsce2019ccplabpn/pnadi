#include <stdio.h>
#include <stdlib.h>
int inp()
{
    int x;
    scanf("%d",&x);
    return x;
}
void input(int r1,int c1,int a[r1][c1])
{
    for(int i=0;i<r1;i++)
    {
        for(int j=0;j<c1;j++)
        {
            printf("Enter a number for position %d,%d\t",i+1,j+1);
            a[i][j]=inp();
        }
    }
}
void comp(int r1,int c1,int a[r1][c1],int b[r1][c1],int c[r1][c1])
{
    for(int i=0;i<r1;i++)
    {
        for(int j=0;j<c1;j++)
        {
            c[i][j]=a[i][j]-b[i][j];
        }
    }
}
void disp(int r1,int c1,int a[r1][c1])
{
    for(int i=0;i<r1;i++)
    {
        for(int j=0;j<c1;j++)
        {
            printf("%d\t",a[i][j]);
        }
        printf("\n");
    }
}
int main()
{
    int r1,c1,r2,c2;
    printf("Enter the size of matrix 1\n");
    r1=inp();
    c1=inp();
    printf("Enter the size of matrix 2\n");
    r2=inp();
    c2=inp();
    if(r1!=r2 || c1!=c2)
    {
        printf("Matrices are of unequal sizes");
        exit(0);
    }
    int a[r1][c1],b[r1][c1],c[r1][c1];
    printf("Enter Matrix A\n");
    input(r1,c1,a);
    printf("Enter Matrix B\n");
    input(r1,c1,b);
    comp(r1,c1,a,b,c);
    printf("Matrix A\n");
    disp(r1,c1,a);
    printf("Matrix B\n");
    disp(r1,c1,b);
    printf("Matrix C\n");
    disp(r1,c1,c);
    return 0;
}
